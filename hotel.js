
// Insert a single room (insertOne method) in the rooms collection

db.room.insert({
	name: "single",
	accommodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	room_available: 10,
	isAvailable: false
});

// Insert multiple rooms (insertMany method)

db.room.insertMany([
   			{
   				name: "double",
   				accommodates: 3,
   				price: 2000,
   				description: "A room fit for a small family going on a vacation",
   				rooms_available: 5,
   				isAvailable: false
   			},

   			{
   				name: "queen",
   				accommodates: 4,
   				price: 4000,
   				description: "A room with a queen sized bed perfect for a simple gateway",
   				rooms_available: 15,
   				isAvailable: false
   			}

	]);

// find method to search for a room with the name double.

db.room.find({ name: "double"});


// updateOne method to update the queen room and set the available rooms to 0.

db.room.updateOne(

		{name: "queen"},
		{
			$set: {
				rooms_available: 0
			}
		}
	);

// deleteMany method to delete all rooms that have 0 rooms available.

db.room.deleteMany(
		{rooms_available: 0}
	 );

// find all rooms

db.getCollection('room').find({});